// # Final Pong

//Comentarios privados

/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Pau Blesa Puig]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"
#include "stdio.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
        // TODO: Define required variables here..........................(0.5p)
        // NOTE: Here there are some useful variables (should be initialized)
    GameScreen screen = LOGO;
    const int velocidady = 10;
    const int ballRadius = 25;
    const int maxvelocity = 10;
    const int minvelocity = 4.5;
    bool pause;
    bool blink;
    bool win = false;
    bool lose = false;
    
    int winNum = 0;
    int loseNum = 0;
    
    int score1p = 0;
    int score2p = 0;
       
       Rectangle palaizquierda;
    
    palaizquierda.width = 20;
    palaizquierda.height = 70;    
    palaizquierda.x = screenWidth - 725 - palaizquierda.width;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
    int palaizquierdaSpeedY = 10;
    
        Rectangle paladerecha;
    
    paladerecha.width = 20;
    paladerecha.height = 70;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
    int paladerechaSpeedY;
    
    Rectangle backRect = { screenWidth/2 - 385, screenHeight/2 - 200, 300, 30}; 
    int margin = 5;
    
    Rectangle fillRect = { backRect.x + margin, backRect.y + margin, backRect.width - (2 * margin), backRect.height - (2 * margin)};
    Rectangle lifeRect = fillRect;
    
    Color lifeColor = YELLOW;
    
    Rectangle backRect2 = { screenWidth/2 + 85, screenHeight/2 - 200, 300, 30}; 
    int margin2 = 5;
    
    Rectangle fillRect2 = { backRect2.x + margin2, backRect2.y + margin2, backRect2.width - (2 * margin2), backRect2.height - (2 * margin2)};
    Rectangle lifeRect2 = fillRect2;
    
    Color lifeColor2 = GREEN;
    
    Vector2 ballPosition;
       ballPosition.x = screenWidth/2;
       ballPosition.y = screenHeight/2;
    
    Vector2 ballSpeed;
        ballSpeed.x = minvelocity;
        ballSpeed.y = minvelocity;
    
    
    //int playerLife;  
    //int enemyLife;
    
    Color logoColor = WHITE;
    
    int secondsCounter = 99;
   // int secondsCounterdifference;
   //   secondsCounterdifference = secondsCounter +1;
    
    
    int framesCounter = 0;          // General pourpose frames counter
    int framesCounter2 = 0;
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    int iaLinex = screenWidth/2;
    
     float fadeOut = true;
     float fadeIn = true;
     float alpha = 0;
     float fadeSpeed = 0.01f;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
             framesCounter++;

                if (framesCounter > 300)  
                {
                    screen = TITLE;
                    framesCounter = 0;
                }
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                 if(fadeOut)
        {
            alpha += fadeSpeed;
           
            if(alpha >= 1.5f)
            {
                alpha = 1.5f;
                fadeOut = !fadeOut;
            }
        }
        else
        {
            alpha -= fadeSpeed;
            if(alpha <= 0.0f)
            {
                alpha = 0.0f;
                fadeOut = !fadeOut;
            }
        }
            }

            break;
            case TITLE: 
            
             framesCounter++;
            if (framesCounter % 30 == 0)
            {
                framesCounter = 0;
                blink = !blink;
        }
            {
                // Update TITLE screen data here!
             if(fadeIn)
        {
            alpha += fadeSpeed;
           
            if(alpha >= 1.0f)
            {
                alpha = 1.0f;
                fadeOut = !fadeOut;
            }
        }

            if (IsKeyPressed (KEY_ENTER))
            {
                screen = GAMEPLAY;
            }
                // TODO: Title animation logic.......................(0.5p)              
                // TODO: "PRESS ENTER" logic.........................(0.5p)               
           
            } 

              
            break;
            case GAMEPLAY:
            { 
            
            framesCounter++;
                // Update GAMEPLAY screen data here!
                // TODO: Ball movement logic.........................(0.2p)
       if(!pause){
            ballPosition.x += ballSpeed.x;
            ballPosition.y += ballSpeed.y;
        }
        
                // TODO: Player movement logic.......................(0.2p)
                if (IsKeyDown(KEY_Q)){
          palaizquierda.y -= palaizquierdaSpeedY;
        }
        
        if (IsKeyDown(KEY_A)){
          palaizquierda.y += palaizquierdaSpeedY;
          
          
        }

        if(pause){
            if (IsKeyDown(KEY_Q)){
              palaizquierda.y += velocidady;
            }
        }
        if(pause){
            if (IsKeyDown(KEY_A)){
              palaizquierda.y -= velocidady;
            
            }
        }
                // TODO: Enemy movement logic (IA)...................(1p)
        if( ballPosition.x > iaLinex){
            if(ballPosition.y > paladerecha.y){
                paladerecha.y+=velocidady;
            }
           
            if(ballPosition.y < paladerecha.y){
                paladerecha.y-=velocidady;
            }
        }
       
       
      
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                 if(palaizquierda.y<0){
            palaizquierda.y = 0;
        }
        
        if(palaizquierda.y > (screenHeight - palaizquierda.height)){
            palaizquierda.y = screenHeight - palaizquierda.height;
        }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if(paladerecha.y<0){
            paladerecha.y = 0;
        }
        
        if(paladerecha.y > (screenHeight - paladerecha.height)){
            paladerecha.y = screenHeight - paladerecha.height;
        }
        
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
        if(ballPosition.x > screenWidth - ballRadius){
            score1p++;
            ballPosition.x = screenWidth/2;
            ballPosition.y = screenHeight/2;
            ballSpeed.x = -minvelocity;
            ballSpeed.y = minvelocity;
           
        }else if(ballPosition.x < ballRadius){
            score2p++;
            ballPosition.x = screenWidth/2;
            ballPosition.y = screenHeight/2;
            ballSpeed.x = minvelocity;
            ballSpeed.y = minvelocity;
        }
       
        if((ballPosition.y > screenHeight - ballRadius) || (ballPosition.y < ballRadius) ){
            ballSpeed.y *=-1;
        }
 
 
        if(CheckCollisionCircleRec(ballPosition, ballRadius, paladerecha)){
            if(ballSpeed.x>0){
                if(abs(ballSpeed.x)<maxvelocity){                    
                    ballSpeed.x *=-1.0;
                    ballSpeed.y *= 1.0;
                }else{
                    ballSpeed.x *=-1;
                }
            }
        }
       
        if(CheckCollisionCircleRec(ballPosition, ballRadius, palaizquierda)){
            if(ballSpeed.x<0){
                if(abs(ballSpeed.x)<maxvelocity){                    
                    ballSpeed.x *=-1.5;
                    ballSpeed.y *= 1.5;
                }else{
                    ballSpeed.x *=-1;
                }
            }
        }
                
                // TODO: Life bars decrease logic....................(1p)
            if (!win && !lose)
        {       
            if (lifeRect.width <= 0)
            {
                lose = true;
                ++loseNum;
                lifeRect.width = 0;        
            }
           
            else if (lifeRect.width >= fillRect.width)
            {
                win = true;
                ++winNum;
                lifeRect.width = fillRect.width;
            }
           
           
            if (lifeRect.width >= 2*(fillRect.width/3))
            {
                lifeColor = GREEN;
            }
           
            else if (lifeRect.width <= fillRect.width/3)
            {
                lifeColor = ORANGE;
            }
           
            else
            {
                lifeColor = YELLOW;
            }
        }
                   
        
                   if (!win && !lose)
        {       
            if (lifeRect2.width <= 0)
            {
                lose = true;
                ++loseNum;
                lifeRect2.width = 0;        
            }
           
            else if (lifeRect2.width >= fillRect2.width)
            {
                win = true;
                ++winNum;
                lifeRect2.width = fillRect2.width;
            }
           
           
            if (lifeRect2.width >= 2*(fillRect2.width/3))
            {
                lifeColor2 = GREEN;
            }
           
            else if (lifeRect2.width <= fillRect2.width/3)
            {
                lifeColor2 = ORANGE;
            }
           
            else
            {
                lifeColor2 = YELLOW;
            }
        }  
                // TODO: Time counter logic..........................(0.2p)
                 if (framesCounter == 60)
                {
                    secondsCounter--;
                     framesCounter = 0;   
                  if (pause) {
                      secondsCounter++;
                      framesCounter = 0;
                  }      

                     }
               
                if (secondsCounter < 0) screen = ENDING;
                
                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                    pause = !pause;
                                        }   
                
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                if (IsKeyPressed(KEY_ENTER))
                {
                    screen=TITLE;
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    DrawRectangle(0,0,screenWidth, screenHeight, BLACK);// Draw LOGO screen here!
                    DrawText("BlesaStudios\n",screenHeight/2, screenWidth/2 -200,50,Fade(WHITE, alpha));
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawRectangle(0,0, screenWidth, screenHeight, BLACK);
                    DrawText("FINAL PONG\n",screenWidth/2 - 310, screenHeight/2 - 175, 100,Fade (RED, alpha ));
                     if (blink) DrawText("Press Start",screenWidth/2 - 185, screenHeight/2, 50,Fade (RED, alpha));
                    // TODO: Draw Title..............................(0.2p)
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangle(0,0,screenWidth, screenHeight, BLACK);
                    DrawCircleV(ballPosition, ballRadius, GREEN);                
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(palaizquierda, BLUE);
                    
                    DrawRectangleRec(paladerecha, ORANGE);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                       DrawRectangleRec (fillRect, RED);
                       DrawRectangleRec (lifeRect, lifeColor);
                       DrawRectangleRec (fillRect2, RED);
                       DrawRectangleRec (lifeRect2, lifeColor2);
                       DrawText(FormatText("SCORE 1P: %d",score1p), 10, 410, 40, RED);
                       DrawText(FormatText("SCORE 2P: %d",score2p), screenWidth - 260, 410, 40, RED);
                    
                    // TODO: Draw time counter.......................(0.5p)
                     DrawText(FormatText("%i", secondsCounter), 370, 50, 50, MAROON); 
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause) {
                    DrawRectangle (0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });
                    DrawText("PAUSED GAME", screenHeight/2, screenWidth/2 - 200, 50, WHITE);
                    }  
                                
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawRectangle(0,0,screenWidth, screenHeight, BLACK);
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    //if (win) {
                    DrawText("You WIN!!\nAwesome!", screenHeight/2 - 10, screenWidth/2 - 325, 80, GREEN);
                    DrawText("EXIT",screenHeight/2 + 100, screenWidth/2 - 100, 50, GREEN);
                    
                    //}
                    if (secondsCounter < 0) {
                    DrawText("You LOSE...\nI'm sorry...", screenHeight/2 - 25, screenWidth/2 - 325, 80, DARKBLUE);
                    DrawText("Replay\n", screenHeight/2 + 90, screenWidth/2 - 100, 50, DARKBLUE);   
                    }
                    
                    
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}